#!/bin/sh
export npm_config_yes=true 

rmdir artifacts > /dev/null 2>&1 || true
mkdir artifacts
cd artifacts

git clone 'https://github.com/piuccio/cowsay.git'

g4c clone 'gh/piuccio/cowsay' cowsay2

cd cowsay
echo "Touching grass..."
touch grass.md
g4c status

echo "Touched grass..."
g4c add --all

echo "Writing an autograssy..."
g4c commit -m 'I, touched, grass...'
# echo "Pushing cows..."
# g4c push
cd - 


echo "Cloning a branch directly"
g4c clone 'https://github.com/feathersjs/playground/tree/template-starter' 'feathers-starter'

echo "auto-cloning demostration"
mkdir typicode-rocks
cd typicode-rocks
g4c clone 'https://github.com/typicode/lowdb' './'
cd -


# Cloning pr's is possible but a bit tricky because they can't diverge
# g4c clone https://github.com/kat-tax/vslite/pull/16
